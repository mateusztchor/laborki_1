﻿using System;
using System.Collections.Generic;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        public IList<IMalpa> Metodajakas()
        {
            Impl1 cos1 = new Impl1();
            Impl1 cos2 = new Impl1();
            Impl2 cos3 = new Impl2();
            Impl2 cos4 = new Impl2();

            List<IMalpa> lista = new List<IMalpa> { cos1,cos2,cos3,cos4 };
            return lista;
        }

        public void Metodajakas2(List<IMalpa> malpy)
        {
            for (int i = 0; i < malpy.Count; i++)
            {
                malpy[i].chodzi();
            }

        }
        static void Main(string[] args)
        {
            Impl3 obiekt = new Impl3();
            Console.WriteLine((obiekt as IMalpa).chodzi());
            Console.WriteLine(obiekt.chodzi());
            Console.WriteLine(obiekt.skacze());
            Console.WriteLine("Za każdym razem powinna być inna liczna");
            
        }
    }
}
