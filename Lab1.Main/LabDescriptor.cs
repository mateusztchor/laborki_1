﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IMalpa);
        
        public static Type ISub1 = typeof(IGoryl);
        public static Type Impl1 = typeof(Impl1);
        
        public static Type ISub2 = typeof(IPawian);
        public static Type Impl2 = typeof(Impl2);
        
        
        public static string baseMethod = "chodzi";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "jesc";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "pic";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Metodajakas";
        public static string collectionConsumerMethod = "Metodajakas2";

        #endregion

        #region P3

        public static Type IOther = typeof(IOlbrzym1);
        public static Type Impl3 = typeof(Impl3);

        public static string otherCommonMethod = "chodzi";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
